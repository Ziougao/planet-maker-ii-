def update(node):

    #load City Selection
    citySelection = node.parm("citySelection").eval()
    #print("citySelection: %s" % citySelection)
        
    #Reset City Booleans
    node.parm("NYCBool").set(0)
    node.parm("RIOBool").set(0)
    node.parm("NAIBool").set(0)
    node.parm("HKBool").set(0)
                
    #Set Selection
    if citySelection == 3:
        node.parm("NYCBool").set(1)
    
    if citySelection == 2:
        node.parm("RIOBool").set(1)
    
    if citySelection == 1:
        node.parm("NAIBool").set(1)
    
    if citySelection == 0:
        node.parm("HKBool").set(1)
        
def updateVis(node):
    
    #load Visualization Selection
    
    visSelection1 = node.parm("DataVis01").eval()
    #print("1th Data to Visualize = %s" % visSelection1)
    visSelection2 = node.parm("DataVis02").eval()
    #print("2nd Data to Visualize = %s" % visSelection1)
    visSelection3 = node.parm("DataVis03").eval()
    #print("3th Data to Visualize = %s" % visSelection1)
        
    #Set the parameters to the selected dataset
    if visSelection1 == "NONE":
        node.parm("Vis01_Toggle").set(0)
        print("No selected Dataset")
        
    else:
        node.parm("Vis01_Toggle").set(1)
        SelectionName1 = "DataName_" + visSelection1
        SelectionScale1 = "ScaleFactor_" + visSelection1
        SelectionColor1r = "DataColor_" + visSelection1 + "r"
        SelectionColor1g = "DataColor_" + visSelection1 + "g"
        SelectionColor1b = "DataColor_" + visSelection1 + "b"
                                
        node.parm("Vis01_DataName").set(str(node.parm(SelectionName1).eval()))
        node.parm("Vis01_ScaleFactor").set(str(node.parm(SelectionScale1).eval()))
        node.parm("Vis01_DataColorr").set(str(node.parm(SelectionColor1r).eval()))
        node.parm("Vis01_DataColorg").set(str(node.parm(SelectionColor1g).eval()))
        node.parm("Vis01_DataColorb").set(str(node.parm(SelectionColor1b).eval()))
        
    if visSelection2 == "NONE":
        node.parm("Vis02_Toggle").set(0)
        print("No selected Dataset")
        
    else:
        node.parm("Vis02_Toggle").set(1)
        SelectionName2 = "DataName_" + visSelection2
        SelectionScale2 = "ScaleFactor_" + visSelection2
        SelectionColor2r = "DataColor_" + visSelection2 + "r"
        SelectionColor2g = "DataColor_" + visSelection2 + "g"
        SelectionColor2b = "DataColor_" + visSelection2 + "b"
                                
        node.parm("Vis02_DataName").set(str(node.parm(SelectionName2).eval()))
        node.parm("Vis02_ScaleFactor").set(str(node.parm(SelectionScale2).eval()))
        node.parm("Vis02_DataColorr").set(str(node.parm(SelectionColor2r).eval()))
        node.parm("Vis02_DataColorg").set(str(node.parm(SelectionColor2g).eval()))
        node.parm("Vis02_DataColorb").set(str(node.parm(SelectionColor2b).eval()))
                        
    if visSelection3 == "NONE":
        node.parm("Vis03_Toggle").set(0)
        print("No selected Dataset")
        
    else:
        node.parm("Vis03_Toggle").set(1)
        SelectionName3 = "DataName_" + visSelection3
        SelectionScale3 = "ScaleFactor_" + visSelection3
        SelectionColor3r = "DataColor_" + visSelection3 + "r"
        SelectionColor3g = "DataColor_" + visSelection3 + "g"
        SelectionColor3b = "DataColor_" + visSelection3 + "b"
                                
        node.parm("Vis03_DataName").set(str(node.parm(SelectionName3).eval()))
        node.parm("Vis03_ScaleFactor").set(str(node.parm(SelectionScale3).eval()))
        node.parm("Vis03_DataColorr").set(str(node.parm(SelectionColor3r).eval()))
        node.parm("Vis03_DataColorg").set(str(node.parm(SelectionColor3g).eval()))
        node.parm("Vis03_DataColorb").set(str(node.parm(SelectionColor3b).eval()))    		
		
		
		