
#_____________________________________________#
##########    MOBILITY GROUP CODE    ##########
#_____________________________________________#


node = hou.pwd()
geo = node.geometry()
groups = geo.primGroups()

import os
import networkx as nx
import math as mt
import random as rd

prims = []
neighbours = []
graph = nx.Graph()
primNums = []
setWeight = []
centrality = []


#### Setting the vehicles you want to be on within the simulation.


hyperloop = node.parm('Hyperloop').eval()
cars = node.parm('Cars').eval()
boats = node.parm('Boats').eval()
planes = node.parm('Planes').eval()
spacePlanes = node.parm('SpacePlanes').eval()

hyp = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_hyperloop')
car = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_car')
boat = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_boat')
plane = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_plane')
spacePlane = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_spacePlane')

if hyperloop == 0:
    hyp.bypass(True)       
else:
    hyp.bypass(False)
    
if cars == 0:
    car.bypass(True)   
else:
    car.bypass(False)
    
if boats == 0:
    boat.bypass(True)   
else:
    boat.bypass(False)
    
if planes == 0:
    plane.bypass(True)   
else:
    plane.bypass(False)
    
if spacePlanes == 0:
    spacePlane.bypass(True)   
else:
    spacePlane.bypass(False)

#### getting all the primitives (houdini points with all the data stored in them with HEX-Values)

for prim in geo.prims():
    prims.append(prim)

    
#### setting a geometric nx-graph from nodeIndex, neighbours and travelweight (A needed graph to be able to calculate with pathfinder algorithms)

for prim in prims:
    if prim.attribValue('landuse_id') > 0 and prim.attribValue('altitude') < 1000:
        index = prim.intrinsicValue('indexorder')
        nbrs = prim.intListAttribValue('primnbrs')
        travelFactor = prim.attribValue('travelFactor')
        for neigh in nbrs:
            graph.add_edge(index, neigh, weight=travelFactor)
            if  index != len(prims):
                continue

        
                
#### getting all houdini primgroups (the different primitive groups within the project)
                
primGroups = []                

for group in groups:
    primGroups.append(group)

#### Difining which groups are the continents that will be calculated over with pathfinding 
    
    
cont0 = primGroups[0]
cont1 = primGroups[1]
cont2 = primGroups[2]
cont3 = primGroups[3]

cont0Prims = cont0.prims()
cont1Prims = cont1.prims()
cont2Prims = cont2.prims()
cont3Prims = cont3.prims()

pathlist0 = []
pathlist1 = []
pathlist2 = []
pathlist3 = []

graph0 = nx.Graph()
graph1 = nx.Graph()
graph2 = nx.Graph()
graph3 = nx.Graph()

otherNodes0 = []    
pathlists0 = []
otherNodes1 = []    
pathlists1 = []
otherNodes2 = []    
pathlists2 = []
otherNodes3 = []    
pathlists3 = []

#### Setting the gdpMin that a triangle need to be able to be included in the network (per vehicle) 

gdpMin = node.parm('gdpMin').eval()

#### all networkx graph nodes (getting all the primitives within the networkx graph)
        
graphNodes = []
for nodes in graph.nodes():
    graphNodes.append(nodes)    

#### continent pathfinders (finding the paths if the conditions are met)(per vehicle)
        

i = 0
while i < 4:

    continent = 'cont' + str(i) + 'Prims'
    otherNodes = eval('otherNodes' + str(i))
    graph = eval('graph' + str(i))
    pathlists = eval('pathlists' + str(i))
    
    for p in eval(continent):
        if p.attribValue('landuse_id') > 0 and p.attribValue('altitude') < 1000:
            index = p.intrinsicValue('indexorder')
            nbrs = p.intListAttribValue('primnbrs')
            travelFactor = p.attribValue('travelFactor')
            for neigh in nbrs:
                graph.add_edge(index, neigh, weight=travelFactor)
        if p.attribValue('GDP') > gdpMin:
           otherNodes.append(p)

        
    
    for p in otherNodes:
        sourceNode = p.intrinsicValue('indexorder')
        for p in otherNodes:
            otherNode = p.intrinsicValue('indexorder')
            if sourceNode != otherNode:
                pathlists.append(nx.dijkstra_path(graph, sourceNode, otherNode))         
    i+=1 

pathlistsTotal = []

pathlistsTotal.append(pathlists0)
pathlistsTotal.append(pathlists1)
pathlistsTotal.append(pathlists2)
pathlistsTotal.append(pathlists3)

otherNodesTotal = []

for nodes in otherNodes0:
    otherNodesTotal.append(nodes)
for nodes in otherNodes1:
    otherNodesTotal.append(nodes)
for nodes in otherNodes2:
    otherNodesTotal.append(nodes)
for nodes in otherNodes3:
    otherNodesTotal.append(nodes)
    

#### finding centrality (adding points for every time a primitive is passed over by a path (per vehicle))

centValues = dict.fromkeys(otherNodesTotal, 0)
minSteps = node.parm('minSteps').eval()
maxSteps = node.parm('maxSteps').eval()

for list in pathlistsTotal:
    for path in list:
        pathlist = path[1:]
        if len(pathlist) > minSteps and len(pathlist) < maxSteps:
            for pathNode in pathlist:
                for o in otherNodesTotal:            
                    if o.intrinsicValue('indexorder') == pathNode:
                        for key, value in centValues.iteritems():
                            if key.intrinsicValue('indexorder') == pathNode:
                                centValues[key] +=1
                                 
for key, value in centValues.iteritems():
    for o in otherNodesTotal:
        if key.intrinsicValue('indexorder') == o.intrinsicValue('indexorder'):    
            o.setAttribValue('Centrality', float(value))  



#### Centrality add up (Gets all the centralities for the different vehicles and adds them up.)


setter = hou.node('/obj/geo1/Group_Mobility__double_click/Vehicle_setter')

#### getting booleans

hyperloop = setter.parm('Hyperloop').eval()
cars = setter.parm('Cars').eval()
boats = setter.parm('Boats').eval()
planes = setter.parm('Planes').eval()
spacePlanes = setter.parm('SpacePlanes').eval()

#### getting the calculators

hyp = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_hyperloop')
car = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_car')
boat = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_boat')
plane = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_plane')
spacePlane = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_spacePlane')

hypGeo = hyp.geometry()
carGeo = car.geometry()
boatGeo = boat.geometry()
planeGeo = plane.geometry()
spacePlaneGeo = spacePlane.geometry()

hypPrims = []
carPrims = []
boatPrims = []
planePrims = []
spacePlanePrims = []

for prim in hypGeo.prims():
    hypPrims.append(prim)
for prim in carGeo.prims():
    carPrims.append(prim)
for prim in boatGeo.prims():
    boatPrims.append(prim)
for prim in planeGeo.prims():
    planePrims.append(prim)
for prim in spacePlaneGeo.prims():
    spacePlanePrims.append(prim)
  
#### adding up centralities    

i = 0  

for prim in geo.prims():
    centralityTotal = 0.0
    if hyperloop != 0:
        centralityTotal += hypPrims[i].attribValue('Centrality')
    if cars != 0:
        centralityTotal += carPrims[i].attribValue('Centrality')
    if boats != 0:
        centralityTotal += boatPrims[i].attribValue('Centrality')
    if planes != 0:
        centralityTotal += planePrims[i].attribValue('Centrality')
    if spacePlanes != 0:
        centralityTotal += spacePlanePrims[i].attribValue('Centrality')
    prim.setAttribValue('Centrality', centralityTotal)
    i += 1
    
#### GDP setter (reconfigures the gdp in a specific given ammount of timebound steps.)


solver = hou.node('/obj/geo1/Group_Mobility__double_click/Mobility_simulation')

iterations = solver.parm('Iterations').eval()

randFloat = rd.uniform

primGroups = []                

for group in groups:
    primGroups.append(group)   
    
graphGroup = primGroups[4]

totalPrims = []

for prim in graphGroup.prims():
    totalPrims.append(prim)

gdpTotal = 0.0 
centralityTotal = 0.0

for prim in totalPrims:
    centralityTotal += float(prim.attribValue('Centrality'))
    gdpTotal += prim.attribValue('GDP')

for prim in geo.prims():
    cPercentage = prim.attribValue('Centrality')/centralityTotal
    gdpEnd = cPercentage * gdpTotal
    gdpStart = prim.attribValue('GDP')
    gdpDifference = gdpEnd - gdpStart
    gdpPerStep = gdpDifference / iterations
    prim.setAttribValue('GDP', prim.attribValue('GDP') + gdpPerStep) 
      
    #Rather logarithmic but doesn't work?:
    #if gdpStart != 0.0:
        #gdpStart * mt.exp(mt.log(mt.exp(1), (gdpEnd / gdpStart) - 10)) * i


#### Calculating the centrality Score between 0 and 100.

centralityMax = 0.0

for prim in geo.prims():
    if prim.attribValue('Centrality') > centralityMax:
        centralityMax = prim.attribValue('Centrality')
print(centralityMax)
        
for prim in geo.prims(): 
    score = (prim.attribValue('Centrality') / centralityMax) * 100
    prim.setAttribValue('CentralityScore', score)
    
#### Calculating emissions based on the selected vehicle.


setter = hou.node('/obj/geo1/Group_Mobility__double_click/Vehicle_setter')

hyperloop = setter.parm('Hyperloop').eval()
cars = setter.parm('Cars').eval()
boats = setter.parm('Boats').eval()
planes = setter.parm('Planes').eval()
spacePlanes = setter.parm('SpacePlanes').eval()

hyp = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_hyperloop')
car = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_car')
boat = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_boat')
plane = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_plane')
spacePlane = hou.node('/obj/geo1/Group_Mobility__double_click/Centrality_Calculator_spacePlane')

co2All = 0.0
vehiclesAmmount = 0.0
iterations = hou.node('/obj/geo1/Group_Mobility__double_click/Mobility_simulation')
iter = iterations.parm('Iterations').eval()

if hyperloop == 1:
    for prim in geo.prims():
        multFact = 0.40**(1.0/iter)
        co2 = prim.attribValue('ChangingDownScaledco2')
        co2M = co2 * 0.25
        co2Down = co2M - (multFact * co2M)
        prim.setAttribValue('ChangingDownScaledco2', co2 - co2Down)
    vehiclesAmmount += 1    

if cars == 1:
    for prim in geo.prims():
        multFact = 1.3**(1.0/iter)
        co2 = prim.attribValue('ChangingDownScaledco2')
        co2M = co2 * 0.25
        co2Down = co2M - (multFact * co2M)   
        prim.setAttribValue('ChangingDownScaledco2', co2 - co2Down)
        if co2 != 0.0:
            print(co2, co2M, co2Down)
    vehiclesAmmount += 1  
    
if boats == 1:
    for prim in geo.prims():
        multFact = 1.2**(1.0/iter)
        co2 = prim.attribValue('ChangingDownScaledco2')
        co2M = co2 * 0.25
        co2Down = co2M - (multFact * co2M)
        prim.setAttribValue('ChangingDownScaledco2', co2 - co2Down)   
    vehiclesAmmount += 1  
    
if planes == 1:    
    for prim in geo.prims():
        multFact = 2**(1.0/iter)
        co2 = prim.attribValue('ChangingDownScaledco2')
        co2M = co2 * 0.25
        co2Down = co2M - (multFact * co2M)
        prim.setAttribValue('ChangingDownScaledco2', co2 - co2Down)
    vehiclesAmmount += 1  
    
if spacePlanes == 1:
    for prim in geo.prims():
        multFact = 4**(1.0/iter)
        co2 = prim.attribValue('ChangingDownScaledco2')
        co2M = co2 * 0.25
        co2Down = co2M - (multFact * co2M)
        prim.setAttribValue('ChangingDownScaledco2', co2 - co2Down)
    vehiclesAmmount += 1

        

   
