## Mobility
------

This directory has been made by the mobility group.

The main purpose of the content of this directory, is to show what the choice of\
annihilating or adding new vehicle types, will bring planet earth.

The influences within the scope of this research are:

* CO<sub>2</sub> emissions
* Local Centrality
* Global Relative Centrality (0 - 100)
* Gdp distribution

The above aspects are visualized on a Global scale in the form of extrutions of\
triangles on a subdivided platonic solid. And on a city scale by extruding data\
on top of buildings, or as a total of all buildings, in the middle of the city cutout.
