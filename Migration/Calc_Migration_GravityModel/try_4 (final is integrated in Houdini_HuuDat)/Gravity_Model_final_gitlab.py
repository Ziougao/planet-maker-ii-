import numpy as np
from scipy import inf
import pandas as pd
import csv
from csv import reader
from scipy import inf
from scipy.sparse.csgraph import floyd_warshall
from numpy import array, asarray, inf, zeros, minimum, diagonal, newaxis
from numpy.random import randint

######################## INPUT DATASETS ##########################

f_1 = open(r"D:\Homework\Planetmaker\Future Models\\Data\Neighborlist.csv") #import neighborlist
adjencies = reader(f_1)
list_adjencies = list(adjencies)
array_adjencies = np.array(list_adjencies, dtype=int)
#print(array_adjencies)

f_2 = open(r"D:\Homework\Planetmaker\Future Models\Data\population_triangle.csv") #import population data
popu_list = list(csv.reader(f_2))
popu_list = popu_list[1:] #delete first row of names
popu_list = np.array(popu_list)
popu_list = popu_list[:, 2] #use second column
popu_array = popu_list.astype(np.float) #convert from strings to floats

f_3 = open(r"D:\Homework\Planetmaker\Future Models\Data\land_water.csv") #import weight (sea)
vals_landwater = reader(f_3)
list_landwater = list(vals_landwater)
array_valslandwater = np.array(list_landwater, dtype=float)
array_landwater = array_valslandwater.ravel() #1D array of 320 nodes: 0 is land, 1 is sea

f_4 = open(r"D:\Homework\Planetmaker\Future Models\\Data\elev_score1.csv") #import weight (altitude)
vals_elev = reader(f_4)
list_elev = list(vals_elev)
array_valselev = np.array(list_elev, dtype=float)
array_elev = array_valselev.ravel() #flattened 1D array of 320 nodes

#import climate_array

###################### LOOPING INPUT AND OUTPUT ###################

iterations = 1 #amount of iterations
for iter in range(iterations):

    input_file_1 = open(r"D:\Homework\Planetmaker\Future Models\Data\Real Data\Labor_Force_" + str(iter + 1) + ".csv") #import production mass for the Gravity Model
    prod_mass_list = list(csv.reader(input_file_1))
    prod_mass_array = np.array(prod_mass_list, dtype=float)

    input_file_2 = open(r"D:\Homework\Planetmaker\Future Models\Data\Real Data\Jobs_Total_" + str(iter + 1) + ".csv") #import attraction mass for the Gravity Model
    attr_mass_list = list(csv.reader(input_file_2))
    attr_mass_array = np.array(attr_mass_list, dtype=float)
    
    ###################### MAKE PATH ##########################

    matrix = np.zeros((320,320)) #zero matrix 320x320

    for i in range(len(matrix)): #make path with neighbors
        neighs = array_adjencies[i]
        for neigh in neighs:
            matrix[i][neigh] = 1

        for j in range(len(matrix)):
            if matrix[i][j] != matrix[i][neigh]:
                matrix[i][j] = inf
            if matrix[i][j] == matrix[i][i]:
                matrix[i][j] = 0

    ############################## ADD WEIGHTS TO THE PATH ##################################

    #option geography
    for row in range(len(matrix)):
        for index in range(len(array_landwater)):
            land = array_landwater[index]
            if land == 0:
                if matrix[row][index] == 1:
                    matrix[row][index] = 20
        
            elevation = array_elev[index]
            if matrix[row][index] == 1:
                matrix[row][index] = elevation

    #option isolationism
    for row in range(len(matrix)):
        for index in range(len(matrix)):
            if matrix[row][index] == 1:
                matrix[row][index] = 25

    #option climate migration
    for row in range(len(matrix)):
        for index in range(len(climate_array)):
            climate_score = climate_array[index]
        if climate_score != 0:
            if matrix[row][index] == 1:
                matrix[row][index] = climate_score

    ############################## shortest path Floyd Warshall ###################################

    def check_and_convert_adjacency_matrix(adjacency_matrix):
        mat = asarray(adjacency_matrix)

        (nrows, ncols) = mat.shape
        assert nrows == ncols
        n = nrows

        assert (diagonal(mat) == 0.0).all()

        return (mat, n)

    def floyd_warshall_fastest(adjacency_matrix):
        '''floyd_warshall_fastest(adjacency_matrix) -> shortest_path_distance_matrix
        Input
        An NxN NumPy array describing the directed distances between N nodes.
        adjacency_matrix[i,j] = distance to travel directly from node i to node j (without passing through other nodes)
        Notes:
        * If there is no edge connecting i->j then adjacency_matrix[i,j] should be equal to numpy.inf.
        * The diagonal of adjacency_matrix should be zero.
        Output
        An NxN NumPy array such that result[i,j] is the shortest distance to travel between node i and node j. If no such path exists then result[i,j] == numpy.inf
        '''
        (mat, n) = check_and_convert_adjacency_matrix(adjacency_matrix)

        for k in range(n):
            mat = minimum(mat, mat[newaxis,k,:] + mat[:,k,newaxis]) 

        return mat
    sp_geodesics = floyd_warshall_fastest(matrix) # shortest path (Cij) without SciPy
  
    ################### AUTOMATION ##################
    ### automation for all triangles ###

    '''Each iteration 2% of Jobs will become automated
    For example, 2% automation = 0.02
    Gives error when automation = 1 (100%)
    This applies to all triangles'''

    percent_allTriangles = 0
    for row in range(len(attr_mass_array)):
        current_jobs = attr_mass_array[row]
        attr_mass_array[row] = current_jobs * (1 - percent_allTriangles)
        attr_mass_array = np.array(attr_mass_array, dtype=np.float)

    ### automation for specific triangles ###  

    def Automation(Percentage, triangle):
        '''
        Automation for specefic triangle
        Attr_Array = Jobs Total
        Percentage = Percentage of Jobs becoming automated, example: 1% = 0.01
        '''
            
        triangle_id = attr_mass_array[triangle]
        attr_mass_array[triangle] = (triangle_id) * (1 - Percentage)
        
        return attr_mass_array

        automation_array = Automation(0, 0) #default

    ### automation in china ###
    
    automation_array = Automation(0.1 , 4)
    automation_array = Automation(0.1 , 5)
    automation_array = Automation(0.1 , 6)
    automation_array = Automation(0.1 , 7)
    automation_array = Automation(0.1 , 14)
    automation_array = Automation(0.1 , 15)
    automation_array = Automation(0.1 , 17)
    automation_array = Automation(0.1 , 64)
    automation_array = Automation(0.1 , 65)
    automation_array = Automation(0.1 , 66)
    automation_array = Automation(0.1 , 67)
    automation_array = Automation(0.1 , 88)
    automation_array = Automation(0.1 , 89)
    automation_array = Automation(0.1 , 90)
    automation_array = Automation(0.1 , 91)
    automation_array = Automation(0.1 , 148)
    automation_array = Automation(0.1 , 149)
    automation_array = Automation(0.1 , 151)
    automation_array = Automation(0.1 , 170)

    #################### CALCULATE SINGLE CONSTRAINT TRIP DISTRIBUTION USING GRAVITY MODEL ######################

    # variant Gravity Model using Production and Attraction (demand and supply):
    # the distance matrix needs to be converted to friction factor
    # Friction Factors f(Cij) =  exp(-0.010*Cij)
    # then compute an new trip distribution

    f_matrix = np.exp(-0.10 * sp_geodesics) #f(Cij) = exp(-0.010 * Cij), f = -0.10 might give the best output

    #################### CALCULATE DOUBLE CONSTRAINT TRIP DISTRIBUTION USING GRAVITY MODEL ######################

    def DoubleConstrained(ProdA, AttrA, F):
        
        '''Calculates doubly constrained trip distribution for a given friction factor matrix
        ProdA = Production array (Labor Force)
        AttrA = Attraction array (Total Jobs)
        F = Friction factor matrix
        Returns Trip Table
        '''
        
        Trip_Matrix = np.zeros((len(ProdA),len(ProdA)))
    
        sum_Prod = sum(ProdA)
        sum_Attr = sum(AttrA)
        print ('Production Total: ', sum_Prod)
        print ('Attraction Total: ', sum_Attr)
        if sum_Prod != sum_Attr:
            print ('Productions and attractions do not balance, Attractions will be scaled to Productions!')
            AttrA = AttrA*(sum_Prod/sum_Attr)
            AttrT = AttrA.copy()
            ProdT = ProdA.copy()
            print(sum(ProdT))
            print(sum(AttrT))
        else:
            print ('Production, attraction balancing OK.')
            AttrT = AttrA.copy()
            ProdT = ProdA.copy()
        
        for i in range(0,len(ProdA)):
            Trip_Matrix[i,:] = ProdA[i]*AttrA*F[i,:]/sum(AttrA * F[i,:])
        return Trip_Matrix
        
    tp_matrix_doubleC = DoubleConstrained(prod_mass_array[:, 0], attr_mass_array[:, 0], f_matrix) #update the max iter to amount of loops

    ########################### CALCULATE MIGRATION #############################

    sum_x = np.sum(tp_matrix_doubleC, axis = 0)
    sum_y = np.sum(tp_matrix_doubleC, axis = 1)
    total_inout = sum_x - sum_y #get the total ins and outs for each country

    production_out = prod_mass_array[:, 0] + (total_inout * 0.5) #change of input due to migration: production masses (labor force/employement)
    attraction_out = attr_mass_array[:, 0] + (total_inout * 0.5)  #attraction output will stay the same
    
    prod_out_arr = np.array(production_out)
    attr_out_arr = np.array(attraction_out)
    output = prod_out_arr
    output_2 = attr_out_arr

    
    #################### LOOPING OUTPUT BACK TO INPUT / OUTPUT ##########################

    output_file = np.savetxt(r"D:\Homework\Planetmaker\Future Models\Data\Real Data\Labor_Force_" + str(iter + 2) + ".csv", output, delimiter=",", fmt="%f")
    output_file_2 = np.savetxt(r"D:\Homework\Planetmaker\Future Models\Data\Real Data\Jobs_Total_" + str(iter + 2) + ".csv", output_2, delimiter=",", fmt="%f")
    input_file_1 = output_file
    input_file_2 = output_file_2

    print(iter+1)
    
    
    
  



