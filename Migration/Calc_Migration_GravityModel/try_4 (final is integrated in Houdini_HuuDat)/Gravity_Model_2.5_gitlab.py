import numpy as np
from scipy import inf
import pandas as pd
import csv
from csv import reader
from scipy import inf
from scipy.sparse.csgraph import floyd_warshall
from numpy import array, asarray, inf, zeros, minimum, diagonal, newaxis
from numpy.random import randint

######################## INPUT DATASETS ##########################

f_1 = open(r"D:\Homework\Planetmaker\Future Models\\Data\Neighborlist.csv") #import neighborlist
adjencies = reader(f_1)
list_adjencies = list(adjencies)
array_adjencies = np.array(list_adjencies, dtype=int)
#print(array_adjencies)

f_2 = open(r"D:\Homework\Planetmaker\Future Models\Data\population_triangle.csv") #import population data
popu_list = list(csv.reader(f_2))
popu_list = popu_list[1:] #delete first row of names
popu_list = np.array(popu_list)
popu_list = popu_list[:, 2] #use second column
popu_array = popu_list.astype(np.float) #convert from strings to floats

f_3 = open(r"D:\Homework\Planetmaker\Future Models\Data\land_water.csv") #import weight (sea)
vals_landwater = reader(f_3)
list_landwater = list(vals_landwater)
array_valslandwater = np.array(list_landwater, dtype=float)
array_landwater = array_valslandwater.ravel() #1D array of 320 nodes: 0 is land, 1 is sea

f_4 = open(r"D:\Homework\Planetmaker\Future Models\\Data\elev_score1.csv") #import weight (altitude)
vals_elev = reader(f_4)
list_elev = list(vals_elev)
array_valselev = np.array(list_elev, dtype=float)
array_elev = array_valselev.ravel() #flattened 1D array of 320 nodes

###################### LOOPING INPUT AND OUTPUT ###################

iterations = 1 #amount of iterations
for iter in range(iterations):

    input_file_1 = open(r"D:\Homework\Planetmaker\Future Models\Data\Real Data\Labor_Force_" + str(iter + 1) + ".csv") #import production mass for the Gravity Model
    prod_mass_list = list(csv.reader(input_file_1))
    prod_mass_array = np.array(prod_mass_list, dtype=float)

    input_file_2 = open(r"D:\Homework\Planetmaker\Future Models\Data\Real Data\Jobs_Total_1.csv") #import attraction mass for the Gravity Model
    attr_mass_list = list(csv.reader(input_file_2))
    attr_mass_array = np.array(attr_mass_list, dtype=float)
    
    ###################### MAKE PATH ##########################

    matrix = np.zeros((320,320)) #zero matrix 320x320

    for i in range(len(matrix)): #make path with neighbors
        neighs = array_adjencies[i]
        for neigh in neighs:
            matrix[i][neigh] = 1

        for j in range(len(matrix)):
            if matrix[i][j] != matrix[i][neigh]:
                matrix[i][j] = inf
            if matrix[i][j] == matrix[i][i]:
                matrix[i][j] = 0

    #print(matrix)

    ############################## ADD WEIGHTS TO THE PATH ##################################
    
    """mask_sea = (matrix == array_landwater) & (matrix != inf) & (matrix != 0) #check if the node is sea -> change weight (sea is 1, land is 0)
    matrix[mask_sea] = 20 #make the weight at sea 20"""

    """for row in range(len(matrix)):
        for index in range(len(array_landwater)):
            land = array_landwater[index]
            if land == 0:
                if matrix[row][index] == 1:
                    matrix[row][index] = 20
        
            elevation = array_elev[index]
            if matrix[row][index] == 1:
                matrix[row][index] = elevation"""

    ############################## shortest path Floyd Warshall ###################################

    #floyd_warshall(matrix) #use this if SciPy works, if not then use the two defs below

    #sp_geodesics = floyd_warshall(matrix) # shortest path (Cij)

    def check_and_convert_adjacency_matrix(adjacency_matrix):
        mat = asarray(adjacency_matrix)

        (nrows, ncols) = mat.shape
        assert nrows == ncols
        n = nrows

        assert (diagonal(mat) == 0.0).all()

        return (mat, n)

    def floyd_warshall_fastest(adjacency_matrix):
        '''floyd_warshall_fastest(adjacency_matrix) -> shortest_path_distance_matrix
        Input
        An NxN NumPy array describing the directed distances between N nodes.
        adjacency_matrix[i,j] = distance to travel directly from node i to node j (without passing through other nodes)
        Notes:
        * If there is no edge connecting i->j then adjacency_matrix[i,j] should be equal to numpy.inf.
        * The diagonal of adjacency_matrix should be zero.
        Output
        An NxN NumPy array such that result[i,j] is the shortest distance to travel between node i and node j. If no such path exists then result[i,j] == numpy.inf
        '''
        (mat, n) = check_and_convert_adjacency_matrix(adjacency_matrix)

        for k in range(n):
            mat = minimum(mat, mat[newaxis,k,:] + mat[:,k,newaxis]) 

        return mat
    sp_geodesics = floyd_warshall_fastest(matrix) # shortest path (Cij) without SciPy
  
    #print(sp_geodesics[0])
    

    #################### CALCULATE SINGLE CONSTRAINT TRIP DISTRIBUTION USING GRAVITY MODEL ######################

    # variant Gravity Model using Production and Attraction (demand and supply):
    # the distance matrix needs to be converted to friction factor
    # Friction Factors f(Cij) =  exp(-0.010*Cij)
    # then compute an new trip distribution (single contrained)

    f_matrix = np.exp(-0.10 * sp_geodesics) #f(Cij) = exp(-0.010 * Cij), f = -0.10 might give the best output

    #print(f_matrix[0])

    """def SingleConstrained(Prod_Array, Attr_Array, F):
        
        '''Calculates singly constrained trip distribution for a given friction factor matrix
        Prod_Array = Production array (Labor Force)
        Attr_Array = Attraction array (Total Jobs)
        F = Friction factor matrix
        Returns trip table
        '''

        SumAjFij = (Attr_Array * F).sum(1) 
        SumAjFij[SumAjFij == 0] = 0.0001
        return Prod_Array * (Attr_Array * F).transpose() / SumAjFij

    tp_matrix = SingleConstrained(prod_mass_array[:,0], attr_mass_array[:,0], f_matrix) 
    tp_matrix_singleC = tp_matrix.transpose()

    #print(tp_matrix_singleC)"""

    #################### CALCULATE DOUBLE CONSTRAINT TRIP DISTRIBUTION USING GRAVITY MODEL ######################

    def DoubleConstrained(ProdA, AttrA, F):
        
        '''Calculates doubly constrained trip distribution for a given friction factor matrix
        ProdA = Production array (Labor Force)
        AttrA = Attraction array (Total Jobs)
        F = Friction factor matrix
        Returns Trip Table
        '''
        
        Trip_Matrix = np.zeros((len(ProdA),len(ProdA)))
    
        sum_Prod = sum(ProdA)
        sum_Attr = sum(AttrA)
        print ('Production Total: ', sum_Prod)
        print ('Attraction Total: ', sum_Attr)
        if sum_Prod != sum_Attr:
            print ('Productions and attractions do not balance, Attractions will be scaled to Productions!')
            AttrA = AttrA*(sum_Prod/sum_Attr)
            AttrT = AttrA.copy()
            ProdT = ProdA.copy()
            print(sum(ProdT))
            print(sum(AttrT))
        else:
            print ('Production, attraction balancing OK.')
            AttrT = AttrA.copy()
            ProdT = ProdA.copy()
        
        for i in range(0,len(ProdA)):
            Trip_Matrix[i,:] = ProdA[i]*AttrA*F[i,:]/sum(AttrA * F[i,:])
        return Trip_Matrix
        
    tp_matrix_doubleC = DoubleConstrained(prod_mass_array[:, 0], attr_mass_array[:, 0], f_matrix) #update the max iter to amount of loops

    ########################### CALCULATE MIGRATION #############################

    sum_x = np.sum(tp_matrix_doubleC, axis = 0)
    sum_y = np.sum(tp_matrix_doubleC, axis = 1)
    total_inout = sum_x - sum_y #get the total ins and outs for each country

    production_out = prod_mass_array[:, 0] + (total_inout * 0.5) #change of input due to migration: production masses (labor force/employement)
    attraction_out = attr_mass_array[:, 0] + (total_inout * 0.5)  #attraction output will stay the same
    
    prod_out_arr = np.array(production_out)
    attr_out_arr = np.array(attraction_out)
    output = prod_out_arr
    output_2 = attr_out_arr

    #popu_tot = popu_array + total_inout #population change
    #popu_tot_arr = np.array(popu_tot)
    #popu_output = popu_tot_arr

    
    #################### LOOPING OUTPUT BACK TO INPUT / OUTPUT ##########################

    #tp_matrix_csv = np.savetxt(r"D:\Homework\Planetmaker\Future Models\Data\Real Data\tp_matrix_" + str(iter) + ".csv", tp_matrix_doubleC, delimiter=",", fmt="%f")
    output_file = np.savetxt(r"D:\Homework\Planetmaker\Future Models\Data\Real Data\Labor_Force_" + str(iter + 2) + ".csv", output, delimiter=",", fmt="%f")
    output_file_2 = np.savetxt(r"D:\Homework\Planetmaker\Future Models\Data\Real Data\Jobs_Total_" + str(iter + 2) + ".csv", output_2, delimiter=",", fmt="%f")
    input_file_1 = output_file
    input_file_2 = output_file_2

    print(iter+1)
    



    #np.savetxt(r"D:\Homework\Planetmaker\Future Models\Data\Fake CSV Out\output_" + str(iter + 1) + ".csv", output , delimiter=",")
    #np.savetxt(r"D:\Homework\Planetmaker\Future Models\Data\Fake CSV Out\matrix" + str(iter + 2) + ".csv", tp_matrix_doubleC, delimiter=",")
    #np.savetxt(r"D:\Homework\Planetmaker\Future Models\Data\Fake CSV Out\total_inout_" + str(iter + 1) + ".csv", total_inout , delimiter=",")
    
    
  



