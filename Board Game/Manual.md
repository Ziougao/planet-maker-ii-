# NAME
_Forest Farms Folk_\
_2-4 players_


## CONTENT OF THE GAME

**the board** \
1 tetrahedral dice \
4 x 15 dark green forest pins \
4 x 15 brown agriculture pins \
4 x 15 population pins \
… chance cards \
60 emission blocks \

### _Diagram with explanation of the objects_


## GOAL

As leader of a section of the world, you are responsible for housing and feeding your population whilst monitoring the emission output of your section

Survive and finish the game with the least EMISSION and the most POPULATION


## BOARD SETUP

Unfold the board

Explanation board picture

The board has 320 triangles, each triangle is a ‘region’. Each region has a land use type: water, green, desert or snow. The game is played by placing a various type of pins in the holes on each region. There are three types of pins: dark green, representing forest, brown, representing agriculture and white, representing population. Each pin has the different player’s color on the head edge.

Each player picks his playing color and grabs the pins of that color: 15 forest, 15 agriculture and 15 population pins.

Shuffle the chance cards and place them next to the board face down.

 
## GETTING STARTED

Every player throws the dice. The highest throw takes the first turn. Play then continues clockwise. On the first turn every player claims their starting region by placing one population pin on the board. 


## GAMEPLAY

First round
In each round, a player may complete 4 actions.
The first player rolls the dice. The number pointing upwards is the number of population pins that must be placed. Placing each population pin counts as 1 action.

After placing the correct number of population pins, the player may use their remaining actions by choosing from the following options:
-	Place agriculture pins:
You must have one agriculture pin for each population pin in order to sustain your population. Warning: this will be checked!
With each placement of an agriculture pin, you gain one emission block.
-	Place forest pins:
With each placement of a forest pin you lose 1 emission block.

Once the first player has completed their 4 actions, play continues clockwise.

__Subsequent rounds__\
Before rolling the dice, the player picks a chance card. Unless it says you must play immediately, you can save this card to play later. Playing a chance card also uses 1 action.

__Placing population__\
Population can only be placed on the adjacent triangles of your pins. Only with a transport card it is possible to travel further.


## CHANCE CARDS


## SCORE

The one with the highest points wins the game
You get your total score by subtracting the emission blocks from your population pins

