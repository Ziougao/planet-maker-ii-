# What is in the repository?

This is the repository for The Why Factory Planet Maker II Studio. 

## Contributors:
Alex Hewitson, Ziou Gao, Bowen Chen, Yangjie Liu, Yingda Sui, Chris Benning, Ton van Giesen, Leonardo Traverso, Huudat Nguyen, Minnie Chu, Maaike Mossinkoff, Anneloes Van Slooten and Kevin van Weerdenburg

With the help of Hans Hoogenboom, Shervin Azadi, Karim Daw and Pirouz Nourian. 

## Contents:

In this studio we have made a simulation for different topics on the global scale. As the folders suggest, there is a simulation for Food(Diet), Migration, Climate Change and Mobility. 

In this repository you may find python scripts to process raw data, python or c++ scripts of the simulation engines, and several Houdini files with 3d models, the simulation engine and the visualisations. 
An Urban visualisation tool in Houdini is also made, together with a Board Game, representing the digital game we made in Houdini. 

After the seperate engines, we have combined them into one simulation, where each groups inputs and outputs are linked with other group(s). 



