# Git for dummies

Use gitlab for sharing code, houdini files and some zips. You can also do other
files but they don't work nicely in gitlab, so better use Gdrive. 

## Step by step guide to get gitlab running on your own pc: (locally)
1. install git (using Hans' link on brightspace, both mac and pc) \
    it's a back-end thing, so it's normal that you don't see it after installing \
    for mac users, open terminal and try git (if a manual shows up, you're good) \

2. open Terminal(Mac)/Command Prompt(Windows) \
    **type:**
    cd (change directory) \
    **cd Documents** let's you work in Documents Folder \
    Make a folder to put the repository in (somewhere) and **cd** to that folder

3. get ssh key: (optional)\
    An ssh key (as I understood it) is your DNA on the web. You will get a public and a private key. 
    The public one can be used to link your online stuff to your local so you don't always have to type in your account and password.
    Don't ever give your private key to someone!!

    **type:**
    ssh-keygen

    it will ask you to store it somewhere, just use default and move it later \
    it will ask you for a password to protect it. Make a good one, save it somewhere, so you remember (for later).

    You will see two files: \
    your_filename \
    your_filename.pub

    (.pub is the public one)
4. connecting PUBLIC ssh key to gitlab: \
    when going to gitlab, go to your profile settings, find the ssh tab and add a key. \
    Open your public key file in the text editor and copy all the text to gitlab. 

5. create clone of gitlab repository to your local computer: \
    Click the blue button **clone** in gitlab \
    copy the ssh link\
    **type:**
    git clone [the link]

    you should see some stuff installing

    if it didn't work, I dunno
    try **type:** \
    git clone https://gitlab.com/Ziougao/planet-maker-ii-.git

6. Check if git is installed \
    **type:**
    git status

    If it gives an error, try **type:** \
    git init

    Make sure you're always working in the folder you made earlier

7. Check branches \
    **type:**
    git branch 

    You should see all the branches, the idea is that everyone works in their own branch 

    By default you should be on the master branch
    
7a. Create a branch \
    git branch [branchname]

8. Switch branch \
    **type:**
    git checkout [branchname]

    It should have switched branches from master to your group branch

9. Now you can put in all the files you want in the local folder. \
    Make extra folders as much as you want put always put a random text file in there. (otherwise it doesn't read it)

10. If you open any file in visual studio code, it would read the whole repository and sync it.

    Any changes you made to text files and your code he will know and will say that new changes have been made, but not synced yet. 

11. to sync all changes you made (new folders, new files, updates in files etc.) to the online gitlab

    2 options:
    1. work in terminal
    2. work in VSC (which is a bit more visual)

    there is some stuff you need to do before you can actually upload them. 

    1. Stage it (make it ready to be synced)
    2. Commit + commit message (Make it ready for take off + give a message of what you did: new changes, added new file, fixed stuff)

        If you type it nicely, and if we started this earlier, we would have been able to track what we did when. 

12. This will be for if you want to work in the terminal (because I need pics to explain how it works in VSC)

    **type:**
    git status \
    (check what kind of new files there are to upload)

    **type:**
    git add [filename or foldername for multiple files] \
    (staging the files)
    
    **type:**
    git commit -m "message you want to write" \
    (this message will be for all the files you staged, so stage 1 by 1 if these should be seperate messages)

    **type:**
    git push \

    If everything went correctly (which probably not), it should ask either for your ssh password (see step 3) or your gitlab account and password. 

13. If other people made changes in the same branch \
    **type:**
    git pull

    This should pull all the changes made in that branch (make sure you're working on the right branch (see step 8)) to your local computer (it will just refresh it with new file)

    Then you can work again

# tl;dr

1. Make new folder for repository
2. cd [Folder location]
1. git clone [url]
1. git init (if necessary)
2. git status
3. git branch
4. git checkout [group branch name]
5. Move, change, add files
6. git add [filenames or folder names for multiple files]
7. git commit -m "message you want to type" 
8. git push







    