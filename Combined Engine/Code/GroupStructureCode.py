#_____________________________________________#
##########    GROUPCOMBINING CODE    ##########
#_____________________________________________#


node = hou.pwd()
geo = node.geometry()

#### getting all the primitives (houdini points with all the data stored in them with HEX-Values)

prims = [] 
for prim in geo.prims():
    prims.append(prim)

#### getting user imput (setting a paramater to downscale the initial geometry to a geometry with less primitives)

downScaleValue = node.parm('downScaleValue').eval()

#### making groups to downscale (setting the amount of triangles to store within one big triangle)

list = []
lists = []
i = 0
for prim in prims:
    list.append(prim)
    i += 1
    if i == downScaleValue:
        lists.append(list)
        i = 0
        list = []


#### making new data lists that store the downscaled group data into the Geometry.)

scaledValue = 0

scaledValues = []
j = 1

while j < 17:
            
        for list in lists:
            for l in list:
                scaledValue += l.attribValue(node.parm('atrib' + str(j)).eval())
                i += 1
                if i == downScaleValue:
                    scaledValues.append(scaledValue)
                    i = 0
                    scaledValue = 0
                   
                    
        newAttribute = "DownScaled" + str(node.parm('atrib' + str(j)).eval())                 
        geo.addAttrib(hou.attribType.Prim, newAttribute, 0.0)
        
        for prim in prims:
            index = prim.intrinsicValue('indexorder')
            if index < len(scaledValues):        
                prim.setAttribValue(newAttribute, scaledValues[i])
                i += 1
                
                
                
        split = hou.node('/obj/geo1/split1')         
        split.parm('group').set(str('0-' + str(20479 / downScaleValue)))
        
        j += 1
        i = 0
        scaledValues = []
        scaledValue = 0
        if node.parm('atrib' + str(j)).eval() == "None":
            break
        

#### making a second list from the downscaled list to later calculate the change over time.

attribute = hou.node('/obj/geo1/DOWNSCALER_MOBILITY')

j = 1

while j < 17:
    firstAttribute = 'DownScaled' + str(attribute.parm('atrib' + str(j)).eval())
    
    
    newAttribute = "ChangingDownScaled" + str(attribute.parm('atrib' + str(j)).eval())
          
    geo.addAttrib(hou.attribType.Prim, newAttribute, 0.0)
    
    for prim in geo.prims():
        prim.setAttribValue(newAttribute, prim.attribValue(firstAttribute))
    j+=1
    if attribute.parm('atrib' + str(j)).eval() == "None":
            break
    


#### Calculating the change over time and after deleting the unnecessary comparing data.

node = hou.pwd()
geo = node.geometry()

attribute = hou.node('/obj/geo1/DOWNSCALER_MOBILITY')

prims = [] 
for prim in geo.prims():
    prims.append(prim)

j = 1
deletiongroup2 = 'primnbrs'
while j < 17:
    firstAttribute = 'DownScaled' + str(attribute.parm('atrib' + str(j)).eval())
    secondAttribute = "ChangingDownScaled" + str(attribute.parm('atrib' + str(j)).eval())
    
    newAttribute = 'DifferenceIn' + str(attribute.parm('atrib' + str(j)).eval())
    
    geo.addAttrib(hou.attribType.Prim, newAttribute, 0.0)
    deletiongroup2 += " "
    deletiongroup2 += firstAttribute
    deletiongroup2 += " "
    deletiongroup2 += secondAttribute
    deletiongroup2 += " "
    deletiongroup2 += newAttribute
    deletiongroup2 += " "
    
    for prim in prims: 
        if prim.attribValue(firstAttribute) != 0:
            prim.setAttribValue(newAttribute, prim.attribValue(secondAttribute) / prim.attribValue(firstAttribute)) 

    j+=1
    if attribute.parm('atrib' + str(j)).eval() == "None":
            break   
print(deletiongroup2)            
attributeDeleteNode = hou.node('/obj/geo1/attribdelete1')

attributeDeleteNode.parm('primdel').set(deletiongroup2) 



#### Giving the initial primitives the same change as the downscaled overruling primitive.

node = hou.pwd()
geo = node.geometry()

attribute = hou.node('/obj/geo1/DOWNSCALER_MOBILITY')

prims = [] 
for prim in geo.prims():
    prims.append(prim)
    
    
downscaler = hou.node('/obj/geo1/DOWNSCALER_MOBILITY')
downScaleValue = downscaler.parm('downScaleValue').eval()
    
list = []
lists = []
i = 0
for prim in prims:
    list.append(prim)
    i += 1
    if i == downScaleValue:
        lists.append(list)
        i = 0
        list = []
 
small = hou.node('/obj/geo1/Change_Calculator1')
smallGeo = small.geometry()


j = 1
while j < 17:
    
    geos = []
    for geo in smallGeo.prims():
        geos.append(geo.attribValue('DifferenceIn'  + str(downscaler.parm('atrib' + str(j)).eval())))
            
    
    k = 0
    
    for list in lists:
        for l in list:
            l.setAttribValue(str(downscaler.parm('atrib' + str(j)).eval()), l.attribValue(str(downscaler.parm('atrib' + str(j)).eval()))* geos[i])
            k += 1
            if k == downScaleValue:
                i += 1
                k = 0
    j+=1
    i = 0
    if attribute.parm('atrib' + str(j)).eval() == "None":
        break 
            

         
            
