#!/usr/bin/env python
# coding: utf-8

# In[1]:


### NOTE: Where you see this kind of code it means you have to change something in the code to make it work ###


# In[44]:


#Importing Several libraries
from csv import reader as read
import pandas as pd
import numpy as np
from IPython.display import display


# In[58]:


### NOTE: Put the path to your CSV file in here in <YOUR PATH AND NAME OF CSV FILE>                                                            

#Load your CSV file per country in here
dfYourData = pd.read_csv(r"C:<YOUR PATH AND NAME OF CSV FILE>.csv", nrows=5)
display(dfYourData)


# In[8]:


### NOTE: Put the path to the Country_Info_V2.csv file in here <YOUR PATH AND NAME OF CSV FILE>                                             

#Importing a CSV file with many different ways of naming a country
dfCountryNames = pd.read_csv(r"C:<YOUR PATH>\Country_Info_V2.csv", nrows=5)
display(dfCountryNames)


# In[ ]:


### NOTE: Rename the two strings below to the coresponding column name below in <YOURDATANAME> and <NAMEFROMLISTBELOW>                              

#Renaming the name of the column with countries in YourData
dfYourData.rename(columns = {"<YOURDATANAME>":"<NAMEFROMLISTBELOW>"}, inplace=True)


# In[19]:


### NOTE: Change the name acording to the name you chose above <NAMEFROMLISTBELOW>                                            

#Merging YourData to the list with all the country names
dfData_CoutryID = pd.merge(dfCountryNames, dfYourData, on='<NAMEFROMLISTBELOW>', how='outer')


# In[7]:


### NOTE: Change the path to the CSV file to your own path in <YOURPATHHERE>                                            

#Importing a CSV file with percentages per land on which triangles
dfTriangles = pd.read_csv(r"C:<YOURPATHHERE>\Percentage_per_Country_per_Triangle.csv")

#Dropping the first column because it's not needed
dfTriangles.drop(dfTriangles.columns[0], axis=1, inplace=True)


# In[26]:


### NOTE: Change the name acoring to the name of your data in <NAMEOFYOURVALUE>

#Mergeing YourData with the percentages and keeping only necessary columns
dfTriangles_Data = pd.merge(dfData_CoutryID, dfTriangles, on='CountryID', how='outer')
dfTriangles_Data.drop(dfTriangles_Data.columns.difference(['CountryID','TriangleID','percentages', '<NAMEOFYOURVALUE>']), 1, inplace=True)


# In[27]:


### NOTE: Change the name acoring to the name of your data in <NAMEOFYOURVALUE> (same name as above)

#Calculating the percentages of YourData
dfTriangles_Data['valuePerTriangle']= dfTriangles_Data.apply(lambda x: x['<NAMEOFYOURVALUE>'] * x['percentages'] / 100, axis = 1)


# In[54]:


#Calculating the Data from YourData to triangle
dftriangle_Data = dfTriangles_Data[['TriangleID','valuePerTriangle']].groupby(['TriangleID'])['valuePerTriangle'].sum()                 .reset_index(name='valuePerTriangle')


# In[57]:


### NOTE: Change the path of your choise and choose a name for your new CSV file and change in <YOURPATH> and <FILENAME>
### NOTE: Do not choose the same filename as your original CSV as this wil overwrite it

#Exporting TriangleData to CSV
triangleData.to_csv(r"C:<YOURPATH>\<FILENAME>.csv")


# In[ ]:




