//Getting attributes from Houdini Details
float totalgdp = detail(0,"totalgdp");
float totalpop = detail(0,"totalpop");

//Calculating the Global GDP per capita for each itteration
float GDPavgc = float(totalgdp/totalpop);
//Making a variable form primitive attributes
float New_gdp = @New_gdp;
float New_pop = @New_pop;

//Calculate the GDP per capita
@GDPc = (New_gdp/New_pop);
//Calculation ased on the estimation of the UN on population in 2060 and the ratio between GDP per capita
//then the calculated growfactor for 2060 for 40 itterations is 100.00145 every year. Based on the estimation of 
@Next_pop = (((GDPavgc/@GDPc) * 0.00145) + 1) * @Next_pop;
//calculating the percentage of growth and making it an attribute to calculate the growth of agriculture
@change_pop = @Next_pop / @Start_pop;