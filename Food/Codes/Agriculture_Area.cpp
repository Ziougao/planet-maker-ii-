//Calculating the area of agriculture acording to the production of different food types

//AREA OF AGRICULTURE starting posistion
float CerealsA = (@Cereals * 0.83 );
float DairyA = ((@Dairy/10000) * 14 );
float EggsA = (@Eggs * 14.5 );
float FruitA = (@Fruit * 2.7 );
float BeefA = (@Beef * 72 );
float PorkA = (@Pork * 46.63 );
float ChickenA = (@Chicken * 65.5);
float VegA = (@Veg * 1.4);

float Total_A = (CerealsA + DairyA + EggsA + FruitA + BeefA + PorkA + ChickenA + VegA);

@PercA_Cereals = (CerealsA * 100) / Total_A;
@PercA_Dairy = (DairyA * 100) / Total_A;
@PercA_Eggs = (EggsA * 100) / Total_A;
@PercA_Fruit = (FruitA * 100) / Total_A;
@PercA_Beef = (BeefA * 100) / Total_A;
@PercA_Pork = (PorkA * 100) / Total_A;
@PercA_Chicken = (ChickenA * 100) / Total_A;
@PercA_Veg = (VegA * 100) / Total_A;

//BIOTECH If we do biotech we multiply this with a factor simulate the slower growth
//@Agri_P = @Agri_P * 0.1;

//NEW AREA OF AGRICULTURE We calculate area to a percentage of the area on the 20480 model
float Area_Agri = @Agri_P;
@A_Cereals = Area_Agri * (@PercA_Cereals/100);
@A_Dairy = Area_Agri * (@PercA_Dairy/100);
@A_Eggs = Area_Agri * (@PercA_Eggs/100);
@A_Fruit = Area_Agri * (@PercA_Fruit/100);
@A_Beef = Area_Agri * (@PercA_Beef/100);
@A_Pork = Area_Agri * (@PercA_Pork/100);
@A_Chicken = Area_Agri * (@PercA_Chicken/100);
@A_Veg = Area_Agri * (@PercA_Veg/100);

//calculate the new percentage of agriculture in the triangle
@Agri_P = ((@A_Cereals + @A_Dairy + @A_Eggs + @A_Fruit + @A_Beef + @A_Pork + @A_Chicken + @A_Veg));

//Manually change this for the different diets
//MEAT
//@Agri_P = ((@A_Beef + @A_Pork + @A_Chicken)*1.2);

//VEGAN
//@Agri_P = ((@A_Cereals*0.63) + @A_Dairy + @A_Eggs + @A_Fruit + (@A_Veg*0.63));

//VEGETARIAN
//@Agri_P = (@A_Cereals*0.75) + @A_Fruit + (@A_Veg*0.75);

//CEREALS
//@Agri_P = (@A_Cereals*0.63);

//VEGETABLES
//@Agri_P = (@A_Veg*0.63);

//translate the change back to percentages to correspond to a factor of 64
counter = 0;
for(int i = 0; i < 64; i++)
     if (@Agri_P < (counter/64))
         {
         @Forest_P += (counter/64);
         @Agri_P =- (counter/64);
         counter =+ i;
         }