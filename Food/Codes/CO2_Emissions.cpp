//CO2 equivalent emissions
//Taking the attribute in Tons of food. Multipliing by the corresponding emmisions factor per foodtype
float SCereals = (@Cereals * 550);
float SDairy = ((@Dairy*0.0001) * 18450);
float SEggs = (@Eggs * 4800);
float SFruit = (@Fruit * 15000);
float SBeef = (@Beef * 30450);
float SPork = (@Pork * 5450);
float SChicken = (@Chicken * 4450);
float SVeg = (@Veg * 2000);

//When simulationg a diet consisting of only one type of food than take that factor and put it behind all of the food types
//Everyone eats cereals
// float SCereals = (@Cereals * 550);
// float SDairy = ((@Dairy*0.0001) * 550);
// float SEggs = (@Eggs * 550);
// float SFruit = (@Fruit * 550);
// float SBeef = (@Beef * 550);
// float SPork = (@Pork * 550);
// float SChicken = (@Chicken * 550);
// float SVeg = (@Veg * 550);

//Making a variable to get the start amount of CO2 emissions by somming up all of the different types of food
float Start_CO2Agri = (SCereals + SDairy + SEggs + SFruit + SBeef + SPork + SChicken + SVeg);

//Correcting data because some datasets dont match so we take the avarage growth of population and give that to the Change_pop when there is agriculture
if (@change_pop == 0.0 && Start_CO2Agri > 0)
     {
     @change_pop = 1.0005;
     }

//Calculating the Emisions for each itteration by multiplying it with the growth of population percentage
float CO2Cereals = (@Cereals * 550) * @change_pop;
float CO2Dairy = ((@Dairy) * 18450) * @change_pop;
float CO2Eggs = (@Eggs * 4800) * @change_pop;
float CO2Fruit = (@Fruit * 15000) * @change_pop;
float CO2Beef = (@Beef * 30450) * @change_pop;
float CO2Pork = (@Pork * 5450) * @change_pop;
float CO2Chicken = (@Chicken * 4450) * @change_pop;
float CO2Veg = (@Veg * 2000) * @change_pop;

//The different types of diet can be manualy adjusted by making them a notition and deleting the // for the diet you want
//Normal diet
@CO2Agri_New = (CO2Cereals + CO2Dairy + CO2Eggs + CO2Fruit + CO2Beef + CO2Pork + CO2Chicken + CO2Veg);

//Meat
//@CO2Agri_New = (CO2Beef + CO2Pork + CO2Chicken);

//Vegetarian
//The 0.75 is the percentage of the food produced for Human consumption so without food for animals
//@CO2Agri_New = ((CO2Cereals*0.75) + CO2Dairy + CO2Eggs + CO2Fruit + (CO2Veg*0.75));

//Vegan
//The 0.63 is the percentage of the food produced for Human consumption so without food for animals
//@CO2Agri_New = ((CO2Cereals*0.63) +  CO2Fruit + (CO2Veg*0.63));

//Cereals
//@CO2Agri_New = (CO2Cereals*0.63);

//Vegetables
//@CO2Agri_New = CO2Veg;

// float CO2Agri_Change = @CO2Agri - (@CO2Agri_New);
// @CO2_Change = ((CO2Agri_Change*-1)/1000000);

//Calculating the amount of emissions each year
@CO2_Change = (@CO2Agri_New - Start_CO2Agri);